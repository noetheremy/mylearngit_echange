## Licence du projet : GNU General Public License v3.0 or later

**Licence du code source de ce fichier :** GNU General Public License v3.0 or later

Logiciel libre / open source mylearngit_echange
Copyright (C) 2024  Jean-François Mai (alias jfm@)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

---

# Projet libre / open source mylearngit_echange

## Objectifs

Tester, comprendre et apprendre les interactions CLI (via un canal sécurisé SSH utilisant une clé cryptographique privée verrouillée par une phrase de passe de grande entropie) d'un dépôt Git local installé dans le répertoire personnel d'un utilisateur d'un système d'exploitation OpenBSD vers un dépôt Git distant hébergé sur l'instance Gitlab associée à apps.education.fr.

## Langages utilisés pour les tests

- Langage C
- GNU make
- Shell

## Systèmes d'exploitation cibles

- OpenBSD 7.5-stable
- FreeBSD FreeBSD 14.1-RELEASE-p5
- NetBSD 10.0-STABLE
- Debian GNU/Linux 12.7

**Remarque :** Quitte à effectuer quelques modifications, ce programme devrait aussi se compiler sur les systèmes d'exploitation libres / open source Arch Linux et Slackware Linux 15.0.

## Objectifs du code source C

Implémentation du programme classique : permutation de deux nombres entiers !

## Objectifs du code source Perl

Génération de trois fichiers contenant les sommes de contrôles des fichiers de l'arborescence de la racine du projet en utilisant respectivant les algorithmes **MD5**, **SHA256** et **SHA512**.

## Notions utilisées

- Pointeurs
- Allocation dynamique de la mémoire
- Appel système pledge(2) spécifique du système d'exploitation OpenBSD ("minimisation" des appels systèmes)
- Appel système unveil(2) spécifique du système d'exploitation OpenBSD ("confinement" des processus au sein d'un système de fichiers)
- Blibliothèque statique (construction de zéro d'une telle bibliothèque)

## Crédits : 

Ce projet, créé de zéro, from scratch donc, ne dépend que du langage C et de sa bibliothèque standard.

## Comment compiler ce programme ?

        ada$ gmake

## Comment utiliser ce programme ?

        ada$ ./permutation

## Comment nettoyer le répertoire racine de ce programme ?

        ada$ gmake clean

## Vérifications des sommes de contrôles :

Sur un système OpenBSD :

        ada$ md5 -C fingerprint.md5.txt
        ada$ sha256 -C fingerprint.sha256.txt
        ada$ sha512 -C fingerprint.sha512.txt

Sur un système FreeBSD ou Debian GNU/Linux :
        
        ada$ md5sum -c fingerprint.md5.txt
        ada$ sha256sum -c fingerprint.sha256.txt
        ada$ sha512sum -c fingerprint.sha512.txt

Sur un système NetBSD :
        
        ada$ cat fingerprint.md5.txt | cksum -c
        ada$ cat fingerprint.sha256.txt | cksum -c
        ada$ cat fingerprint.sha512.txt | cksum -c

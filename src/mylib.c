/* Licence du code source de ce fichier : GNU General Public License v3.0 or later

*    Le logiciel permutation est un logiciel libre / open source sous licence GNU GPLv3+. 
*    Copyright (C) 2024  Jean-François Mai (alias jfm@)
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.

*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.

*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <stdio.h>

#include "./src/mylib.h"

int
permutIntegers(int* ref_a, int* ref_b) 
{
	int tmp = 0;

	tmp = *ref_a;
	*ref_a = *ref_b;
	*ref_b = tmp;
	
	return 0;
}

// Fonction qui teste si ce qui a ete saisi commence par un nombre entier
// avec un maximum de 5 chiffres : le cas echeant, la partie qui n'est pas
// un nombre entier est ignoree.

int
scanfTestInteger(const char *rang, int* ref_nb) 
{
	int tmp = 0;

	//
	do {
		printf("\n\nQuel le %s nombre entier ?\n", rang);
		// On securise la saisie :
		// (1) On limite la saisie du nombre entier a au plus 5 chiffres.
		// (2) En dessous de 5 chiffres, tout caractere qui n'est pas un
		// chiffre sera ignore.
		tmp = scanf("%5i", ref_nb);
		// En cas d'erreur (tmp different de 1), on prend le soin de vider
		// le buffer de l'entree standard (le clavier dans notre cas), en
		// utilisant le REGEX : [^\n].
		// On envoie dans le neant tous les caracteres de la ligne mis dans
		// le buffer (sauf le retour charriot \n qui termine la dite ligne).
		// On elimine enfin du buffer de l'entree standard le retour chariot \n.
		scanf("%*[^\n]%*c");
	} while (tmp != 1);

	return 0;
}

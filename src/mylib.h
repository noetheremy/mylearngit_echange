#ifndef MYLIB_H
#define MYLIB_H

int permutIntegers(int*, int*);
int scanfTestInteger(const char*, int*);

#endif /* MYLIB_H */

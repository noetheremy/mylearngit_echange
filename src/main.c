/* Licence du code source de ce fichier : GNU General Public License v3.0 or later

*    Le logiciel permutation est un logiciel libre / open source sous licence GNU GPLv3+. 
*    Copyright (C) 2024  Jean-François Mai (alias jfm@)
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.

*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.

*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <err.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h> // OpenBSD pledge / OpenBSD unveil

#include "./src/mylib.h"

#define WRITE_PRIMITIV1	 write(0, "####################################################", 53)
#define WRITE_PRIMITIV2	 write(0, "###################################################", 52)
#define PRINT_MYOS(x, y) printf("\n## Programme %s construit sur %s. ##\n", (x), (y))	

int
main(int argc, char *argv[]) 
{
	if (argc == 1) {
		#ifdef __OpenBSD__
			if (unveil(argv[0], "rwc") == -1) {
				err(1, "unveil");
			}
			if (pledge("stdio", NULL) == -1) {
				err(1, "pledge"); 
			}
			WRITE_PRIMITIV1;
			PRINT_MYOS(argv[0], "OpenBSD");
			WRITE_PRIMITIV1; 
		#endif /* __OpenBSD__ */
		#ifdef __FreeBSD__
			WRITE_PRIMITIV1;
			PRINT_MYOS(argv[0], "FreeBSD");
			WRITE_PRIMITIV1;
		#endif /* __FreeBSD__ */

		#ifdef __NetBSD__
			WRITE_PRIMITIV2;
			PRINT_MYOS(argv[0], "NetBSD");
			WRITE_PRIMITIV2;
		#endif /* __NetBSD__ */
		
		#ifdef __LINUX__
			WRITE_PRIMITIV2;
			PRINT_MYOS(argv[0], "Linux");
			WRITE_PRIMITIV2;
		#endif /* __LINUX__ */
		
		int *ref_a = NULL;
		int *ref_b = NULL;
	
		ref_a = malloc(sizeof(int));
		ref_b = malloc(sizeof(int));

		if (!ref_a) {
			printf("Mince, une erreur d'allocation memoire s'est produite pour ref_a.\n");
			exit(1);
		}
	
		if (!ref_b) {
			printf("Mince, une erreur d'allocation memoire s'est produite pour ref_b.\n");
			exit(1);
		}

		const char *prem = "premier";
		scanfTestInteger(prem, ref_a);
		const char *sec = "second";
		scanfTestInteger(sec, ref_b);

		printf("\n\nAvant permutation : a = %d et b = %d.\n", *ref_a , *ref_b);
		permutIntegers(ref_a, ref_b);
		printf("Permutation faite : a = %d et b = %d.\n", *ref_a , *ref_b);
	
		free(ref_a);
		ref_a = NULL;
		free(ref_b);
		ref_b = NULL; 
	
		return 0;
	} else {
		printf("Fin du programme !\n");
		return 0;
	}
}

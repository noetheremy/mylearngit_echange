# Licence du code source de ce fichier : GNU General Public License v3.0 or later

# Copyright (C) 2024  Jean-François Mai (alias jfm@)

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

use strict;
use warnings;

# variable Perl qui contient le nom de l'OS sous-jacent !
my $os = $^O;
# fichier texte temporaire situe a la racine './' du projet
my $tmp = "./tmp.txt";
# liste vide
my @tab=();

sub hashFingerprint() { 
	if ( $os eq 'freebsd' or $os eq 'netbsd' or $os eq 'openbsd' ) {
		system("find . -type f -exec md5 {} \\; > ./fingerprint.md5.bsd.txt");
		system("find . -type f -exec sha256 {} \\; > ./fingerprint.sha256.bsd.txt"); 
		system("find . -type f -exec sha512 {} \\; > ./fingerprint.sha512.bsd.txt");
	} else {
		system("find . -type f -exec md5sum {} \\; > ./fingerprint.md5.linux.txt");
		system("find . -type f -exec sha256sum {} \\; > ./fingerprint.sha256.linux.txt"); 
		system("find . -type f -exec sha512sum {} \\; > ./fingerprint.sha512.linux.txt");
	}
}

sub parserFingerprint {
	# 1 entree :
	# arg1 ($in) -> fichier au format texte a parser
	# 1 sortie :
	# arg2 ($out) -> le meme fichier texte une fois parsee
	my ($in, $out) = @_;
	
	# OpenBSD / NetBSD / FreeBSD
	my $patternb1 = qr/^(MD5|SHA256|SHA512) \(.\/pdf/;
	my $patternb2 = qr/^(MD5|SHA256|SHA512) \(.\/init\/main.tex\)/;
	my $patternb3 = qr/^(MD5|SHA256|SHA512) \(.\/init\/packages.tex\)/;
	my $patternb4 = qr/^(MD5|SHA256|SHA512) \(.\/src\/credits.tex\)/;
	my $patternb5 = qr/^(MD5|SHA256|SHA512) \(.\/.git\//;
	my $patternb6 = qr/^(MD5|SHA256|SHA512) \(.\/fingerprint\./;
	my $patternb7 = qr/^(MD5|SHA256|SHA512) \(.\/log\//;
	
	# Debian GNU/Linux / Arch Linux / Slackware Linux
	my $patternl = qr/(.pdf|main.tex|packages.tex|credits.tex|.\/.git|bsd.txt|linux.txt|erreurs.txt|standard.txt|main.log|txt.sig)$/;

	open(IN, "< $in") or die "Impossible de lire le fichier $in.\n";
	open(OUT, "> $out") or die "Impossible d'ouvrir et d'écrire dans le fichier $out.\n";

	while (<IN>) {
	# regles REGEX (debut)
		if ( /$patternb1/ or /$patternb2/ or /$patternb3/ or /$patternb4/ or 
			/$patternb5/ or /$patternb6/ or /$patternb7/ or /$patternl/ ) 
		{ } else { print OUT; }	
	# regles REGEX (fin)
	}

	close(IN);
	close(OUT);
	
	unlink($in) or die "Impossible de supprimer le fichier $in !";
	rename($out, $in) or die "Impossible de renommer le fichier $out en $in !";
	chmod(0600, $in) or die "Impossible de changer les droits du fichier $in !";
}

sub sortFingerprint() {
	# 2 entrees :
	# arg1 ($data) -> fichier au format txt 'fingerprint.*.{bsd,linux}txt'
	# arg2 ($ref_tab) -> passage par reference (adresse de la liste associee)
	# 2 sorties : 
	# $data -> fichier au format txt 'fingerprint.*.{bsd,linux}txt' trie 
	# par ordre defini a l'avance par Perl et qui sera toujours effectue
	# de la meme facon
	# $ref_tab -> liste @tab reinitilisee en une liste vide
	my ($data, $ref_tab) = @_;

	# on remplit la liste initialement vide des lignes du fichier texte
	open(IN, "< $data") or die "Impossible de lire le fichier $data.\n";

	while (<IN>) {
		push(@{$ref_tab}, $_);
	}
	# on trie la liste selon un ordre immuable et defini par le langage Perl
	@{$ref_tab}=sort(@{$ref_tab});
	close(IN);
	
	# on reecrit les donnees triees dans le fichier texte initial	
	open(OUT, "> $data") or die "Impossible d'ouvrir et d'écrire dans le fichier $data.\n";
	foreach (@{$ref_tab}) { print OUT $_; }
	close(OUT);
	chmod(0600, $data) or die "Impossible de changer les droits du fichier $data !";

	# on reinitialise la liste fournee par son adresse (passage par reference)	
	@{$ref_tab}=();
}

&main::hashFingerprint();

if ( $os eq 'freebsd' or $os eq 'netbsd' or $os eq 'openbsd' ) {
	# OpenBSD / NetBSD / FreeBSD
	&main::parserFingerprint("./fingerprint.md5.bsd.txt", $tmp);
	&main::sortFingerprint("./fingerprint.md5.bsd.txt", \@tab);
	&main::parserFingerprint("./fingerprint.sha256.bsd.txt", $tmp);
	&main::sortFingerprint("./fingerprint.sha256.bsd.txt", \@tab);
	&main::parserFingerprint("./fingerprint.sha512.bsd.txt", $tmp);
	&main::sortFingerprint("./fingerprint.sha512.bsd.txt", \@tab);

} else {
	# Debian GNU/Linux / Arch Linux / Slackware Linux
	&main::parserFingerprint("./fingerprint.md5.linux.txt", $tmp);
	&main::sortFingerprint("./fingerprint.md5.linux.txt", \@tab);
	&main::parserFingerprint("./fingerprint.sha256.linux.txt", $tmp);
	&main::sortFingerprint("./fingerprint.sha256.linux.txt", \@tab);
	&main::parserFingerprint("./fingerprint.sha512.linux.txt", $tmp);
	&main::sortFingerprint("./fingerprint.sha512.linux.txt", \@tab);
}

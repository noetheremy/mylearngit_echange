# Licence du code source de ce fichier : GNU General Public License v3.0 or later

#    Le logiciel permutation est un logiciel libre / open source sous licence GNU GPLv3+. 
#    Copyright (C) 2024  Jean-François Mai (alias jfm@)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.

#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.

#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

MYPROG = permutation
VERSION = 20241028
MYOS = $(shell uname)

INCLDIR = -I${PWD} -I/usr/include

CPPFLAGS = -DVERSION=\"${VERSION}\"

CFLAGS = -Wall -Wextra -pedantic \
	 -march=native \
	 -Wmissing-prototypes \
	 -Wstrict-prototypes \
	 -Wwrite-strings \
	 ${CPPFLAGS} ${INCLDIR}

#CFLAGS += -std=c17
CFLAGS += -MMD
#CFLAGS += -g

#.PHONY: hash chash clean

ifeq ($(MYOS),OpenBSD)
	MYMAKE = gmake
	#cc = CC -O2 ${CFLAGS}
	CC = clang -O2 ${CFLAGS}
	#CC = egcc -O2 ${CFLAGS}
	RM = rm -rfP
else
ifeq ($(MYOS),FreeBSD)
	MYMAKE = gmake
	#CC = cc -O2 ${CFLAGS}
	CC = clang -O2 ${CFLAGS}
	RM = rm -rfP
else
ifeq ($(MYOS),NetBSD)
	MYMAKE = gmake
	CC = cc -O2 ${CFLAGS}
	RM = rm -rfP
else
ifeq ($(MYOS),Linux)
	MYMAKE = make
	CC = cc -O2 ${CFLAGS}
	RM = rm -rf
endif
endif
endif
endif

progstatic:
	@${MYMAKE} makedir	
	@${MYMAKE} libstatic
	@${CC} ./obj/main.o \
		./lib/mylib.a \
		-o ${MYPROG} 
	@ldd ${MYPROG}
	@${MYMAKE} hash

mylib.o: ./src/mylib.c ./src/mylib.h
	@${CC} -o ./obj/mylib.o -c ./src/mylib.c

main.o: ./src/main.c ./src/mylib.c ./src/mylib.h
	@${CC} -o ./obj/main.o -c ./src/main.c

libstatic: 
	@(${MYMAKE} mylib.o && \
		${MYMAKE} main.o)
	@ar -rv ./lib/mylib.a ./obj/*.o

makedir:
	@(install -d -m 0700 ./lib/ && \
		install -d -m 0700 ./obj/)

hash:
	@perl ./scripts/fingerprint.pl

chash:
	@${MYMAKE} clean
	@${MYMAKE} hash

clean:
	@${RM} ${MYPROG} ./*.core ./lib/ ./obj/
